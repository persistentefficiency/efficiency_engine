#Author: Jason Trager
#Purpose: Test analytics, API
import sys,os
from pe_data_api import API
import json
import pandas
import time
#global variables
api_key = "18960eec-74d2-46f4-bdd2-b7aa86921bed" #Replace with a variable in the future

pe_data = API(api_key) # use key provided by dashboard
panel_list = pe_data.get_panels()
test_panel_id = 'bca1d785-b2dd-4aee-9c09-22317f829bb3'
test_panel = pe_data.get_panel(test_panel_id)
test_circuit_id = test_panel['circuits'][4]['circuit_id']
check = pe_data.get_circuit_metrics('38955098-77b3-4ddc-9f48-4f725f0c8c35')


def load_all_circuit_data(circuit_id = None):
    circuit_data = pe_data.get_circuit(circuit_id)
    metrics = []
    data = pe_data.get_circuit_metrics(circuit_id)
    metrics = metrics + data['metrics']
    pages = data['num_pages']
    for i in range(2,pages):
        data = pe_data.get_circuit_metrics(circuit_id, page  =  i)
        metrics = metrics + data['metrics']
        time.sleep(1)
    circuit_data['metrics'] = metrics
    return circuit_data


def load_single_circuit_to_pandas(circuit_data):
    circuit_df=pandas.read_json(json.dumps(circuit_data['metrics']))
    circuit_df['circuit_id'] = circuit_data['circuit_id']
    circuit_df=circuit_df.set_index(['circuit_id','timestamp'])
    return circuit_df
